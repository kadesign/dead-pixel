var queries = [
    'getelementbyid',
    'events',
    'click+event',
    'es6',
    'event+handling',
    'array',
    'bootstrap',
    'fetch',
    'async+await'
]

var myLoc = window.location.href

function bossMode() {
    const getQuery = function () {
        let index = Math.floor(Math.random() * queries.length)
        return queries[index]
    }

    if (!document.querySelector('.boss')) {
        let frame = document.createElement('iframe')
        let q = getQuery()
        frame.setAttribute('src', 'https://www.google.com/search?igu=1&q=js+' + q)
        frame.classList.add('boss')
        document.body.appendChild(frame)
        frame.onload = () => {
            document.title = 'js ' + q.replace('+', ' ') + ' - Google Search'
            let link = document.querySelector("link[rel*='icon']") || document.createElement('link')
            link.type = 'image/x-icon'
            link.rel = 'shortcut icon'
            link.href = 'https://www.google.com/s2/favicons?domain=www.google.com'
            document.getElementsByTagName('head')[0].appendChild(link)
            window.history.pushState("", "", '/search?q=js+' + q);

            // Close frame
            if (!document.querySelector('.boss-away')) {
                let closeBtn = document.createElement('div')
                closeBtn.classList.add('boss-away')
                closeBtn.onclick = bossMode
                closeBtn.innerHTML = 'X'
                document.body.appendChild(closeBtn)
            }
        }

    } else {
        document.querySelector('.boss').remove()
        document.querySelector('.boss-away').remove()
        let link = document.querySelector("link[rel*='icon']") || document.createElement('link')
        link.type = 'image/x-icon'
        link.rel = 'shortcut icon'
        link.href = 'square-64.ico'
        document.getElementsByTagName('head')[0].appendChild(link)
        document.title = 'Dead Pixel'
        window.history.pushState("", "", '/');
    }
}