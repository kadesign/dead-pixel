const lvls = [
    {
        pxSize: 2,
        pxAmount: 10
    },
    {
        pxSize: 2,
        pxAmount: 9
    },
    {
        pxSize: 2,
        pxAmount: 8
    },
    {
        pxSize: 2,
        pxAmount: 7
    },
    {
        pxSize: 2,
        pxAmount: 5
    },
    {
        pxSize: 2,
        pxAmount: 3
    },
    {
        pxSize: 2,
        pxAmount: 1
    },
    {
        pxSize: 1,
        pxAmount: 10
    },
    {
        pxSize: 1,
        pxAmount: 9
    },
    {
        pxSize: 1,
        pxAmount: 8
    },
    {
        pxSize: 1,
        pxAmount: 7
    },
    {
        pxSize: 1,
        pxAmount: 5
    },
    {
        pxSize: 1,
        pxAmount: 3
    },
    {
        pxSize: 1,
        pxAmount: 1
    }
]

const cnv = document.querySelector('#cnv')

if (cnv.getContext) {
    const ctx = cnv.getContext('2d')
    if (ctx.removeHitRegion || ctx.addHitRegion || ctx.clearHitRegions) {
        let stats = document.querySelector('div.stats')
        stats.innerHTML = '<b>Level:</b> <span class="lvl"></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Remaining:</b> <span class="pixels"></span>'

        let statLvl = document.querySelector('.lvl')
        let statRemaining = document.querySelector('.pixels')

        const cnvW = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth
        const cnvH = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight
        let pxSize, pxAmount, lvl
        let pixels = []

        cnv.setAttribute('width', cnvW)
        cnv.setAttribute('height', cnvH - 54)

        const getLvl = () => {
            lvl = localStorage.getItem('level')
            if (!lvl) {
                lvl = 0
                setLvl()
            }

            pxSize = lvls[lvl].pxSize
            pxAmount = lvls[lvl].pxAmount
            console.log('current level: ' + lvl)
            statLvl.innerHTML = parseInt(lvl) + 1
        }

        const setLvl = () => {
            localStorage.setItem('level', lvl)
        }

        const renderPxs = () => {
            let i = 0

            pixels = []
            ctx.clearRect(0, 0, cnvW, cnvH)
            ctx.clearHitRegions()

            while (i < pxAmount) {
                let posX = Math.floor(Math.random() * cnvW) + 1
                let posY = Math.floor(Math.random() * (cnvH - 54)) + 1
                ctx.beginPath()
                ctx.rect(posX, posY, pxSize, pxSize)
                ctx.fill()
                ctx.addHitRegion({ 'id': i, 'cursor': 'default' })
                pixels.push([posX, posY])
                i++
            }
            console.log('pixels: ' + getRemainingPxs())
            statRemaining.innerHTML = getRemainingPxs()
        }

        const searchPx = e => {
            let clickedPx = e.region

            if (clickedPx) {
                let rectX = pixels[clickedPx][0]
                let rectY = pixels[clickedPx][1]
                ctx.clearRect(rectX, rectY, pxSize, pxSize)
                ctx.removeHitRegion(clickedPx)
                pixels[clickedPx] = undefined
                console.log('pixels left: ' + getRemainingPxs())
                statRemaining.innerHTML = getRemainingPxs()
            }
        }

        const clearProgress = () => {
            lvl = 0
            setLvl()
            getLvl()
            renderPxs()
        }

        const getRemainingPxs = () => {
            let count = 0
            for (let i = 0; i < pixels.length; ++i) {
                if (pixels[i] !== undefined)
                    count++
            }
            return count
        }

        getLvl()
        renderPxs()

        cnv.onclick = () => {
            searchPx(event)
            console.log(pixels)
            if (pixels.every(el => el === undefined)) {
                lvl++
                if (lvl == lvls.length) lvl = 0
                console.log('go to level ' + lvl)
                statLvl.innerHTML = parseInt(lvl) + 1
                pxSize = lvls[lvl].pxSize
                pxAmount = lvls[lvl].pxAmount
                renderPxs()
                setLvl()
            }
        }

        document.onkeydown = e => {
            switch (e.keyCode) {
                case 82:
                    clearProgress()
                    break
                case 192:
                case 220:
                    bossMode()
                    break
            }
        }
    } else {
        let alert = document.createElement('div')
        alert.innerHTML = 'This app requires Experimental Web Platform features in Chrome to work. Enable here: <pre>chrome://flags/#enable-experimental-web-platform-features</pre>'
        alert.style.padding = '20px'
        document.querySelector('#cnv').remove()
        document.querySelector('.stats').remove()
        document.body.appendChild(alert)
    }
}